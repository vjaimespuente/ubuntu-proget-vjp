#!/bin/bash
# ------------------------------------------------------------------
# [vjp] microk8s
#          install microk8s latest
# --- Body --------------------------------------------------------

# Install MicroK8s
echo "MicroK8s will install a minimal, lightweight Kubernetes you can run and use on practically any machine."

sudo snap install microk8s --classic

# Join the group
echo "MicroK8s creates a group to enable seamless usage of commands which require admin privilege. To add your current user to the group and gain access to the .kube caching directory, run the following two commands:"

sudo usermod -a -G microk8s $USER
sudo chown -f -R $USER ~/.kube

echo "*****Now you will need to re-enter the session for the group update to take place******"
# -------

